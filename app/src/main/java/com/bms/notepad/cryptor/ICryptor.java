package com.bms.notepad.cryptor;

public interface ICryptor {
    String TRANSFORMATION = "AES/GCM/NoPadding";
    String ANDROID_KEY_STORE = "AndroidKeyStore";
}
