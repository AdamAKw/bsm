package com.bms.notepad.cryptor;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;

public class Decryptor implements ICryptor {
    SecretKeyUtils secretKeyUtils;


    public Decryptor() throws CertificateException, NoSuchAlgorithmException, KeyStoreException,
            IOException {
        initialize();

    }

    private void initialize() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        secretKeyUtils = new SecretKeyUtils();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public String decryptData(final String alias, final byte[] encryptedData, final byte[] encryptionIv)
            throws NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException, IOException,
            BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException, NoSuchProviderException, UnrecoverableEntryException, KeyStoreException {
        final GCMParameterSpec parameterSpec = new GCMParameterSpec(128, encryptionIv);
        SecretKey key = secretKeyUtils.getSecretKey(alias);

        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, key, parameterSpec);

        return new String(cipher.doFinal(encryptedData), "UTF-8");
    }


}