package com.bms.notepad.cryptor;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class Encryptor implements ICryptor {

    private SecretKeyUtils secretKeyUtils;
    private byte[] encryption;
    private byte[] iv;

    public Encryptor() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        secretKeyUtils = new SecretKeyUtils();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public byte[] encryptText(final String alias, final String textToEncrypt)
            throws NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException, IOException,
            BadPaddingException,
            IllegalBlockSizeException, NoSuchProviderException, InvalidAlgorithmParameterException {

        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);

        cipher.init(Cipher.ENCRYPT_MODE, prepareKey(alias));
        iv = cipher.getIV();
        return (encryption = cipher.doFinal(textToEncrypt.getBytes("UTF-8")));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private SecretKey prepareKey(String alias) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
        try {
            return secretKeyUtils.getSecretKey(alias);

        } catch (Exception e) {
            return secretKeyUtils.getNewSecretKey(alias);
        }
    }


    byte[] getEncryption() {
        return encryption;
    }

    public byte[] getIv() {
        return iv;
    }
}
