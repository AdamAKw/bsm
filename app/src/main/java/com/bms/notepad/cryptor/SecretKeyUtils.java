package com.bms.notepad.cryptor;

import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class SecretKeyUtils {
    private KeyStore keyStore;

    public SecretKeyUtils() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        initialize();
    }

    private void initialize() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        keyStore = getKeyStoreInstance();
    }

    private KeyStore getKeyStoreInstance() throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        KeyStore keyStore;
        keyStore = KeyStore.getInstance(ICryptor.ANDROID_KEY_STORE);
        keyStore.load(null);
        return keyStore;
    }

    SecretKey getSecretKey(final String alias) throws NoSuchAlgorithmException,
            UnrecoverableEntryException, KeyStoreException {
        return ((KeyStore.SecretKeyEntry) keyStore.getEntry(alias, null)).getSecretKey();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @NonNull
    SecretKey getNewSecretKey(final String alias) throws NoSuchAlgorithmException,
            NoSuchProviderException, InvalidAlgorithmParameterException {

        final KeyGenerator keyGenerator = KeyGenerator
                .getInstance(KeyProperties.KEY_ALGORITHM_AES, ICryptor.ANDROID_KEY_STORE);

        keyGenerator.init(prepareKeyGenParameterSpec(alias));

        return keyGenerator.generateKey();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @NonNull
    public SecretKey getNewSecretKeyBiometric(final boolean invalidatedByBiometricEnrollment) throws NoSuchAlgorithmException,
            NoSuchProviderException, InvalidAlgorithmParameterException {

        final KeyGenerator keyGenerator = KeyGenerator
                .getInstance(KeyProperties.KEY_ALGORITHM_AES, ICryptor.ANDROID_KEY_STORE);

        keyGenerator.init(prepareKeyGenParameterSpecBiometric(invalidatedByBiometricEnrollment));

        return keyGenerator.generateKey();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private KeyGenParameterSpec prepareKeyGenParameterSpec(String alias) {
        return new KeyGenParameterSpec.Builder(alias,
                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                .build();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private KeyGenParameterSpec prepareKeyGenParameterSpecBiometric(boolean invalidatedByBiometricEnrollment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return new KeyGenParameterSpec.Builder(ICryptor.ANDROID_KEY_STORE,
                    KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setUserAuthenticationRequired(true)
                    .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                    .setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment)
                    .build();
        }
        return new KeyGenParameterSpec.Builder(ICryptor.ANDROID_KEY_STORE,
                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setUserAuthenticationRequired(true)
                .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                .build();
    }

}
