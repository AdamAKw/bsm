package com.bms.notepad.notepad;

import android.content.Context;
import android.os.Build;
import android.util.Base64;

import androidx.annotation.RequiresApi;

import com.bms.notepad.cryptor.Decryptor;
import com.bms.notepad.cryptor.Encryptor;
import com.bms.notepad.notepad.model.Note;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;

class NoteManager {

    private final DBHelper db;
    private byte[] Iv;
    private ArrayList<Note> notes;
    private String username;

    NoteManager(Context context, String username) {
        this.db = new DBHelper(context);
        this.notes = new ArrayList<>();
        this.username = username;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void createNote(String note) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        note = cryptData(note);
        db.insertContact(note, username, Iv);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private String cryptData(String note) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        Encryptor en = new Encryptor();
        try {
            final byte[] noteEncrypted = en.encryptText(username, note);
                Iv = en.getIv();
            return Base64.encodeToString(noteEncrypted, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void getNotesFromDb() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        notes.clear();
        for(Note noteFromDb : db.getData(username)){
            Note note = new Note();
            note.setContent(decryptData(noteFromDb.getContent(),noteFromDb.iv));
            notes.add(note);
        }
    }
    ArrayList<Note> getNotes(){
        return notes;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private String decryptData(String ecrPassword, byte[] iv) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        Decryptor de = new Decryptor();
        try {
            return de.decryptData(username, Base64.decode(ecrPassword, Base64.DEFAULT), iv);

        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
