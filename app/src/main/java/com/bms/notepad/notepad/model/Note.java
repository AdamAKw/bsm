package com.bms.notepad.notepad.model;


public class Note {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public byte[] iv;
}