package com.bms.notepad.notepad;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.bms.notepad.R;
import com.bms.notepad.UserManagment.PasswordChanger;
import com.bms.notepad.notepad.model.Note;
import com.bms.notepad.notepad.model.NoteAdapter;
import com.bms.notepad.ui.user.LoginActivity;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;

public class NotepadActivity extends AppCompatActivity implements NoteAdapter.ItemClickListener {
    private ArrayList<Note> notes = new ArrayList<>();
    private NoteAdapter noteAdapter;
    private NoteManager noteManager;
    private DBHelper db;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notepad);
        db = new DBHelper(this);
        noteManager = new NoteManager(this, getIntent().getStringExtra("USERNAME"));
        getNotesFromDb();
        notes = noteManager.getNotes();
        final Button newNoteButton = findViewById(R.id.createNewNoteButton);
        final Button changePasswordButton = findViewById(R.id.changePassword);
        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createChangePasswordPopUp();

            }
        });
        newNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPopUp();
            }
        });

        createRecyclerView();
    }


    private void createRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.notesList);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        noteAdapter = new NoteAdapter(this, notes);
        noteAdapter.setClickListener(this);
        recyclerView.setAdapter(noteAdapter);
    }

    private void createPopUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.create_new_note));

        final EditText noteContent = new EditText(this);
        noteContent.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(noteContent);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    noteManager.createNote(noteContent.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                getNotesFromDb();
                notes = noteManager.getNotes();
                noteAdapter.notifyItemInserted(notes.size());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void createChangePasswordPopUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.change_password));

        final EditText newPassword = new EditText(this);
        newPassword.setInputType(InputType.TYPE_CLASS_TEXT);
        newPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        builder.setView(newPassword);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    if (!"".equals(newPassword.getText().toString()) && newPassword.getText().toString().length() >= 10) {
                        PasswordChanger uc = createPasswordChanger(newPassword.getText().toString());
                        if (uc.changeUserPassword()) {
                            dialog.dismiss();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private PasswordChanger createPasswordChanger(String newPasword) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        return new PasswordChanger(getIntent().getStringExtra("USERNAME"), newPasword, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getNotesFromDb() {
        try {
            noteManager.getNotesFromDb();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
