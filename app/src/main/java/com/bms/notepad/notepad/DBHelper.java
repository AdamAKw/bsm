package com.bms.notepad.notepad;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

import com.bms.notepad.notepad.model.Note;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "NotesDB.db";

    public static final String NOTE_TABLE_NAME = "note";
    public static final String NOTE_COLUMN_ID = "id";
    public static final String NOTE_COLUMN_CONTENT = "content";
    public static final String NOTE_COLUMN_USER = "user";
    public static final String NOTE_COLUMN_IV = "iv";


    public static  final String USER_TABLE_NAME = "user";
    public static final String USER_COLUMN_ID = "id";
    public static final String USER_COLUMN_USERNAME = "username";
    public static final String USER_COLUMN_PASSWORD= "password";
    public static final String USER_COLUMN_IV = "iv";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 8);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table note " +
                        "(id integer primary key, content text, user text, iv blob)"
        );
        db.execSQL("create table user " +
                "(id integer primary key, username text, password text, iv blob)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS note");
        db.execSQL("DROP TABLE IF EXISTS user");
        onCreate(db);
    }

    public boolean insertContact (String content, String user, byte[] iv) {
        try {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("content", content);
        contentValues.put("user", user);
        contentValues.put("iv", iv);
        db.insert("note", null, contentValues);
        return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public ArrayList<Note> getData(String user) {
        ArrayList<Note> array_list = new ArrayList<Note>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select content, iv from note where user=\""+user+"\"", null );
        res.moveToFirst();
        while(res.isAfterLast() == false){
            Note note = new Note();
            note.iv = res.getBlob(res.getColumnIndex(NOTE_COLUMN_IV));
            note.setContent(res.getString(res.getColumnIndex(NOTE_COLUMN_CONTENT)));
            array_list.add(note);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, NOTE_TABLE_NAME);
        return numRows;
    }

    public boolean userIsNotExistByUsername(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select id from user where username=\""+username+"\"",null);
        if(res.getCount() <= 0){
            res.close();
            return true;
        }
        res.close();
        return false;
    }

    public boolean insertUser(String username, String password, byte[] iv) {
        try{
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("username", username);
        contentValues.put("password", password);
        contentValues.put("iv", iv);
        db.insert("user", null, contentValues);
        return true;
        } catch (Exception e){
            return false;
        }
    }

    public boolean isUserExist(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select id from user where username=\""+username+"\"",null);
        if(res.getCount() > 0){
            res.close();
            return true;
        }
        res.close();
        return false;
    }

    public byte[] getUserIV(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select iv from user where username=\""+username+"\"",null);
        res.moveToFirst();
        return res.getBlob(0);
    }

    public String getUserPassword(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select password from user where username=\""+username+"\"",null);
        res.moveToFirst();
        return res.getString(0);
    }

    public boolean updateUser(String username, String newPassword, byte[] iv) {
        try{
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("password", newPassword);
        contentValues.put("iv", iv);
        db.update("user",contentValues,"username = \""+username+"\"",null);
        return true;
    } catch (Exception e){
        return false;
    }
    }
}