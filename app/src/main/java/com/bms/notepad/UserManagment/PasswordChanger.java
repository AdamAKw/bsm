package com.bms.notepad.UserManagment;

import android.content.Context;
import android.os.Build;
import android.util.Base64;

import androidx.annotation.RequiresApi;

import com.bms.notepad.cryptor.Encryptor;
import com.bms.notepad.cryptor.SecretKeyUtils;
import com.bms.notepad.notepad.DBHelper;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import static com.bms.notepad.UserManagment.IUserManager.DEFAULT_CREATE_LOGIN_ALIAS;

public class PasswordChanger {
    private final String username;
    private String newPassword;
    private byte[] Iv;
    DBHelper db;

    public PasswordChanger(String username, String newPassword, Context context) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        this.username = username;
        this.newPassword = newPassword;
        db = new DBHelper(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean changeUserPassword() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        cryptData(newPassword);
        return db.updateUser(username,newPassword,Iv);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void cryptData(String password) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        Encryptor en = new Encryptor();
        try {
            final byte[] usernameEncrypted = en.encryptText(DEFAULT_CREATE_LOGIN_ALIAS, password);
            this.newPassword = Base64.encodeToString(usernameEncrypted, Base64.DEFAULT);
            this.Iv = en.getIv();

        } catch (NoSuchAlgorithmException |
                IOException | NoSuchPaddingException | InvalidKeyException e) {
        } catch (
                IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
    }
}
