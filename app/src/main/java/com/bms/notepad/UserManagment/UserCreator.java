package com.bms.notepad.UserManagment;

import android.content.Context;
import android.os.Build;
import android.util.Base64;

import androidx.annotation.RequiresApi;

import com.bms.notepad.notepad.DBHelper;
import com.bms.notepad.cryptor.Encryptor;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class UserCreator implements IUserManager {
    private String username;
    private String password;
    private final Context context;
    DBHelper db;
    private byte[] Iv;

    public UserCreator(String username, String password, Context context) {
        this.username = username;
        this.password = password;
        this.context = context;
        db = new DBHelper(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean createUser() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        cryptData(password);
        if (db.userIsNotExistByUsername(username)) {
            return db.insertUser(username, password, Iv);
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void cryptData(String password) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        Encryptor en = new Encryptor();
        try {
            final byte[] usernameEncrypted = en.encryptText(DEFAULT_CREATE_LOGIN_ALIAS, password);
            this.password = Base64.encodeToString(usernameEncrypted, Base64.DEFAULT);
            this.Iv = en.getIv();

        } catch (NoSuchAlgorithmException |
                IOException | NoSuchPaddingException | InvalidKeyException e) {
        } catch (
                IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
    }
}
