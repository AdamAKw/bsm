package com.bms.notepad.UserManagment;

import android.app.Application;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.bms.notepad.R;
import com.bms.notepad.cryptor.ICryptor;
import com.bms.notepad.cryptor.SecretKeyUtils;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import static androidx.biometric.BiometricPrompt.AuthenticationCallback;
import static androidx.biometric.BiometricPrompt.AuthenticationResult;
import static androidx.biometric.BiometricPrompt.CryptoObject;

public abstract class FingerPrintManager {
    public static final String BIOMETRIC_USER = "biometricUser";
    private BiometricPrompt biometricPrompt;
    private SecretKeyUtils utils;
    private Cipher cipher;
    private FragmentActivity context;

    protected FingerPrintManager(FragmentActivity context) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, NoSuchPaddingException {
        biometricPrompt = createBiometricPrompt(context);
        utils = new SecretKeyUtils();
        cipher = Cipher.getInstance(ICryptor.TRANSFORMATION);
        this.context = context;

    }

    public boolean canAuthenticateByBiometric(Application application) {
        return BiometricManager.from(application).canAuthenticate() == BiometricManager.BIOMETRIC_SUCCESS;
    }

    private BiometricPrompt createBiometricPrompt(FragmentActivity context) {
        return new BiometricPrompt(context, ContextCompat.getMainExecutor(context), new AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);

            }

            @Override
            public void onAuthenticationSucceeded(@NonNull AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                onPurchased();
            }
        });
    }

    private BiometricPrompt.PromptInfo createPromptInfo() {
        return new BiometricPrompt.PromptInfo.Builder()
                .setTitle(context.getString(R.string.prompt_info_title))
                .setSubtitle(context.getString(R.string.prompt_info_subtitle))
                .setConfirmationRequired(false)
                .setNegativeButtonText(context.getString(R.string.prompt_info_use_app_password))
                .build();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void authenticate() throws NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchProviderException {
            initializeCipherForAuthenticate();
        biometricPrompt.authenticate(createPromptInfo(), new CryptoObject(cipher));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initializeCipherForAuthenticate() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        cipher.init(Cipher.ENCRYPT_MODE, utils.getNewSecretKeyBiometric(false));
    }

    public abstract void onPurchased();
}
