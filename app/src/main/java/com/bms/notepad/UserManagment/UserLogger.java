package com.bms.notepad.UserManagment;

import android.content.Context;
import android.os.Build;
import android.util.Base64;

import androidx.annotation.RequiresApi;

import com.bms.notepad.notepad.DBHelper;
import com.bms.notepad.cryptor.Decryptor;


import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class UserLogger implements IUserManager {
    private String username;
    private String password;

    private DBHelper databaseHelper;

    public UserLogger(String username, String password, Context context) {
        this.username = username;
        this.password = password;
        this.databaseHelper = new DBHelper(context);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean logIn() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        if (databaseHelper.isUserExist(username)) {
            return equalsPassword();
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean equalsPassword() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        String ecrPassword = databaseHelper.getUserPassword(username);
        byte[] iv = databaseHelper.getUserIV(username);

        return password.equals(decryptData(ecrPassword, iv));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private String decryptData(String dataToDecrypt, byte[] iv) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        Decryptor decryptor = new Decryptor();
        try {
            byte[] bytesData = Base64.decode(dataToDecrypt, Base64.DEFAULT);
            return decryptor.decryptData(DEFAULT_CREATE_LOGIN_ALIAS, bytesData, iv);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
