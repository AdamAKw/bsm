package com.bms.notepad.ui.user;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.bms.notepad.R;
import com.bms.notepad.UserManagment.FingerPrintManager;
import com.bms.notepad.UserManagment.UserLogger;
import com.bms.notepad.notepad.NotepadActivity;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.crypto.NoSuchPaddingException;

public class LoginActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final EditText usernameEditText = findViewById(R.id.loginUsername);
        final EditText passwordEditText = findViewById(R.id.loginPassword);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        createLoginButton(usernameEditText, passwordEditText, loadingProgressBar);
        createRegisterButton();
        try {
            createFingerButton();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createRegisterButton() {
        final Button registerButton = findViewById(R.id.loginRegisterButton);
        registerButton.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRegisterActivity();
            }
        }));
    }

    private void createLoginButton(final EditText usernameEditText, final EditText passwordEditText, final ProgressBar loadingProgressBar) {
        final Button loginButton = findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                if (!"".equals(getText(usernameEditText)) && !"".equals(getText(passwordEditText))) {
                    try {
                        if (logIn(getText(usernameEditText), getText(passwordEditText))) {
                            openNotepadActivity(getText(usernameEditText));
                        } else {
                            loadingProgressBar.setVisibility(View.INVISIBLE);
                            createValidatorAlert(R.string.something_wrong);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    loadingProgressBar.setVisibility(View.INVISIBLE);
                    createValidatorAlert(R.string.some_filed_is_empty);
                }
            }
        });
    }

    private String getText(EditText usernameEditText) {
        return usernameEditText.getText().toString();
    }

    private void createFingerButton() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, NoSuchPaddingException {
        final FingerPrintManager fingerPrintManager = getFingerPrintManager();
        final Button fingerButton = findViewById(R.id.fingerButton);
        if (fingerPrintManager.canAuthenticateByBiometric(getApplication())) {
            fingerButton.setEnabled(true);
            fingerButton.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {
                    try {
                        fingerPrintManager.authenticate();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            Toast.makeText(this, getString(R.string.setup_lock_screen), Toast.LENGTH_LONG).show();
            fingerButton.setEnabled(false);
        }
    }

    private FingerPrintManager getFingerPrintManager() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, NoSuchPaddingException {
        return new FingerPrintManager(this) {
            @Override
            public void onPurchased() {
                    openNotepadActivity(FingerPrintManager.BIOMETRIC_USER);
            }
        };
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean logIn(String username, String password) throws InterruptedException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        UserLogger userLogger = new UserLogger(username, password, this);
        Thread.sleep(500);
        return userLogger.logIn();
    }

    private void openNotepadActivity(String username) {
        Intent intent = new Intent(this, NotepadActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("USERNAME", username);

        startActivity(intent);
        finish();
    }

    private void openRegisterActivity() {
        Intent intent = new Intent(this, RegisterActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void createValidatorAlert(int short_login) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(short_login));

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

}
