package com.bms.notepad.ui.user;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.bms.notepad.R;
import com.bms.notepad.UserManagment.UserCreator;
import com.bms.notepad.ui.user.LoginActivity;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final Button registerButton = findViewById(R.id.loginRegisterButton);
        final Button loginButton = findViewById(R.id.registerLoginButton);
        final EditText usernameEditText = findViewById(R.id.registerUsername);
        final EditText passwordEditText = findViewById(R.id.registerPassword2);
        final EditText replayPasswordEditText = findViewById(R.id.registerPassword1);

        loginButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getLoginActivity();
            }



        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (!"".equals(usernameEditText.getText().toString()) && !"".equals(passwordEditText.getText().toString()) && !"".equals(replayPasswordEditText.getText().toString())) {
                    if (passwordEditText.getText().toString().equals(replayPasswordEditText.getText().toString())) {
                        try {
                            registerNewUser(usernameEditText.getText().toString(), passwordEditText.getText().toString());
                        } catch (CertificateException e) {
                            e.printStackTrace();
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        } catch (KeyStoreException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        createValidatorAlert(R.string.password_not_match);
                    }
                }
                else {
                    createValidatorAlert(R.string.some_filed_is_empty);
                }
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void registerNewUser(String username, String password) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        if(username.length() >= 4){
            if(password.length() >=10){
                UserCreator userCreator = new UserCreator(username, password, this);
                if(userCreator.createUser()){
                 createValidatorAlert(R.string.success);
                }
                else {
                    createValidatorAlert(R.string.something_wrong);
                }
            }
            else {
                createValidatorAlert(R.string.short_password);
            }
        }
        else {
            createValidatorAlert(R.string.short_login);
        }
    }

    private void createValidatorAlert(int short_login) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(short_login));

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void getLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
